import React from "react";
import {AccountService} from "../services/AccountService";
import {IAccount} from "../models/IAccount";
import {Link} from "react-router-dom";
import {Logger} from "../services/Logger";

export class AccountDetails extends React.Component {

  log: Logger;
  customerId: number;
  accountService: AccountService;
  accounts: IAccount[];
  history: any;

  constructor(props: any) {
    super(props);
    this.customerId = props.match.params['id'];
    this.history = props.history;
    this.accountService = new AccountService();
    this.log = new Logger();
    this.accounts = [];
    this.state = {
      accounts: this.accounts
    }
    this.initialize();
  }

  initialize() {
    this.accountService.getAccountsByCustomer(this.customerId)
    .then((res: IAccount[]) => {
      if(res !== undefined){
        this.accounts = res;
        this.setState({accounts: res}, () => this.render());
      }
    })
    .catch(error => this.log.error(error.message));
  }

  render() {
    return (
        <div className="container">
          <div className="jumbotron pt-2">
            <h2>Accounts by customer</h2>
            <table className="table mt-xl-5">
              <thead className="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Account ID</th>
                <th scope="col">Customer ID</th>
                <th scope="col">Balance</th>
                <th scope="col">Currency</th>
                <th scope="col">Country</th>
                <th scope="col"></th>
              </tr>
              </thead>
              <tbody>
              {this.accounts.map((account, index) => (
                  <tr key={account.id}>
                    <th scope="row">{index + 1}</th>
                    <td>{account.id}</td>
                    <td>{account.customerId}</td>
                    <td>{account.amount}</td>
                    <td>{account.currency}</td>
                    <td>{account.country}</td>
                    <td><Link to={"/transactions/" + account.id} className="btn btn-primary">See transactions</Link></td>
                  </tr>
              ))}
              </tbody>
            </table>
          </div>
          <button className="btn btn-primary pr-5 pl-5" onClick={() => this.history.goBack()}>Back</button>
        </div>
    )
  }

}