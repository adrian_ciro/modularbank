import React from "react";
import {IAccount} from "../models/IAccount";
import {AccountService} from "../services/AccountService";
import {Logger} from "../services/Logger";
import {Link} from "react-router-dom";

export class Accounts extends React.Component {

  log: Logger;
  accounts: IAccount[];
  accountService: AccountService;

  constructor(props: any) {
    super(props);
    this.accountService = new AccountService();
    this.log = new Logger();
    this.accounts = [];
    this.state = {
      accounts: this.accounts
    }
    this.initialize();
  }

  initialize() {
    this.accountService.getAllAccounts()
    .then((res: IAccount[]) => {
      if(res !== undefined){
        this.accounts = res;
        this.setState({accounts: res}, () => this.render());
      }
    })
    .catch(error => this.log.error(error.message));
  }

  render() {
    return (
        <div className="container">
          <div className="jumbotron pt-2">
            <h2>Accounts</h2>

            <table className="table mt-xl-5">
              <thead className="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Customer ID</th>
                <th scope="col">Balance</th>
                <th scope="col">Currency</th>
                <th scope="col"></th>
              </tr>
              </thead>
              <tbody>
              {this.accounts.map((account, index) => (
                  <tr key={account.id}>
                    <th scope="row">{index + 1}</th>
                    <td>{account.customerId}</td>
                    <td>{account.amount}</td>
                    <td>{account.currency}</td>
                    <td>
                      <Link to={"/account-details/"+account.customerId} className="btn btn-primary">See account details</Link>
                    </td>
                  </tr>
              ))}
              </tbody>
            </table>

          </div>
        </div>
    )
  }
}