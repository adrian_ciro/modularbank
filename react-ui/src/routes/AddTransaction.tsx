import React from "react";
import {Logger} from "../services/Logger";
import {TransactionService} from "../services/TransactionService";
import {ITransactionResource} from "../models/api/ITransactionResource";
import {ITransaction} from "../models/ITransaction";

export class AddTransaction extends React.Component {

  log: Logger;
  transactionService: TransactionService;
  transaction: ITransactionResource;
  isSaved: boolean;
  hasError: boolean;

  constructor(props: any) {
    super(props);
    this.transactionService = new TransactionService();
    this.log = new Logger();
    this.isSaved = false;
    this.hasError = false;
    this.transaction = {
      amount: null, currency: null, accountId: null, direction: null, description: null
    };
    this.state = {
      transaction: this.transaction,
      isSaved: this.isSaved,
      hasError: this.hasError
    };
  }

  onChangeInput(e: React.FormEvent<HTMLInputElement>) {
    if (e.currentTarget.id === 'accountId') {
      this.transaction.accountId = Number(e.currentTarget.value);
    } else if (e.currentTarget.id === 'amount') {
      this.transaction.amount = Number(e.currentTarget.value);
    } else if (e.currentTarget.id === 'description') {
      this.transaction.description = e.currentTarget.value;
    }
  }

  onChangeSelection(e: React.FormEvent<HTMLSelectElement>) {
    if (e.currentTarget.id === 'currency') {
      this.transaction.currency = e.currentTarget.value;
    } else if (e.currentTarget.id === 'direction') {
      this.transaction.direction = e.currentTarget.value;
    }
    this.setState({transaction: this.transaction});
  }

  save() {
    if (this.transactionService.isValidTransaction(this.transaction)) {
      this.transactionService.saveTransaction(this.transaction)
      .then((res: ITransaction) => {
        if (res !== undefined) {
          this.isSaved = true;
          this.hasError = false;
          this.setState({saved: true, hasError: false}, () => this.render())
          this.log.info("Transaction saved" + res);
        }
        else{
          this.hasError = true;
          this.isSaved = false;
          this.setState({saved: false, hasError: true}, () => this.render())
          this.log.info("Transaction not saved");
        }
      })
      .catch(error => this.log.error(error.message));
    }
  }

  render() {
    return (
        <div className="container">
          <div className="jumbotron pt-2">
            <h2>Create Transaction</h2>

            <div className="mt-5">
              <form className="pl-lg-5 pr-lg-5 App-form text-center">
                <div className="form-group row">
                  <label htmlFor="AccountId" className="col-sm-4 col-form-label text-right">Account Id</label>
                  <div className="col-sm-8">
                    <input type="number" className="form-control" id="accountId" required onChange={(e) => this.onChangeInput(e)}/>
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="amount" className="col-sm-4 col-form-label text-right">Amount</label>
                  <div className="col-sm-8">
                    <input type="number" className="form-control" id="amount" required onChange={(e) => this.onChangeInput(e)}/>
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="currencies" className="col-sm-4 col-form-label text-right">Currencies</label>
                  <div className="col-sm-8">
                    <select className="form-control" id="currency" onChange={(e) => this.onChangeSelection(e)}>
                      <option/>
                      <option>EUR</option>
                      <option>USD</option>
                      <option>GBP</option>
                      <option>SEK</option>
                    </select>
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="direction" className="col-sm-4 col-form-label text-right">Direction</label>
                  <div className="col-sm-8">
                    <select className="form-control" id="direction" onChange={(e) => this.onChangeSelection(e)}>
                      <option/>
                      <option>IN</option>
                      <option>OUT</option>
                    </select>
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="description" className="col-sm-4 col-form-label text-right">Description</label>
                  <div className="col-sm-8">
                    <input type="text" className="form-control" id="description" required onChange={(e) => this.onChangeInput(e)}/>
                  </div>
                </div>
                <div className="mt-5">
                  <button className="btn btn-primary pr-5 pl-5" onClick={() => this.save()}>Create transaction</button>
                </div>
                <div className="mt-5 alert alert-success" role="alert" hidden={!this.isSaved}>
                  The transaction has been created!
                </div>
                <div className="mt-5 alert alert-danger" role="alert" hidden={!this.hasError}>
                  The transaction was not created. Check if accountId exists or currency is valid!
                </div>
              </form>
            </div>

          </div>
        </div>
    )
  }

}