import React from "react";

import {Logger} from "../services/Logger";
import {ITransaction} from "../models/ITransaction";
import {TransactionService} from "../services/TransactionService";

export class Transactions extends React.Component {

  log: Logger;
  transactions: ITransaction[];
  transactionService: TransactionService;
  accountId: number;
  history: any;

  constructor(props: any) {
    super(props);
    this.accountId = props.match.params['id'];
    this.history = props.history;
    this.transactionService = new TransactionService();
    this.log = new Logger();
    this.transactions = [];
    this.state = {
      transactions: this.transactions
    }
    this.initialize();
  }

  initialize() {
    this.transactionService.getTransactions(this.accountId)
    .then((res: ITransaction[]) => {
      if(res !== undefined){
        this.transactions = res;
        this.setState({transactions: res}, () => this.render())
      }
    })
    .catch(error => this.log.error(error.message));
  }

  render() {
    return (
        <div className="container">
          <div className="jumbotron pt-2">
            <h2>Transactions</h2>

            <table className="table mt-xl-5">
              <thead className="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">ID</th>
                <th scope="col">Account ID</th>
                <th scope="col">Amount</th>
                <th scope="col">Currency</th>
                <th scope="col">Direction</th>
                <th scope="col">Description</th>
              </tr>
              </thead>
              <tbody>
              {this.transactions.map((transaction, index) => (
                  <tr key={transaction.id}>
                    <th scope="row">{index + 1}</th>
                    <td>{transaction.id}</td>
                    <td>{transaction.accountId}</td>
                    <td>{transaction.amount}</td>
                    <td>{transaction.currency}</td>
                    <td>{transaction.direction}</td>
                    <td>{transaction.description}</td>
                  </tr>
              ))}
              </tbody>
            </table>

          </div>
          <button className="btn btn-primary pr-5 pl-5" onClick={() => this.history.goBack()}>Back</button>
        </div>
    )
  }
}