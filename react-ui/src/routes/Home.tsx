import React from "react";
import {DashboardService} from "../services/DashboardService";
import {Logger} from "../services/Logger";
import {IDashboard} from "../models/IDashboard";

export class Home extends React.Component {

  log: Logger;
  dashboard: IDashboard;
  dashboardService: DashboardService;

  constructor(props: any) {
    super(props);
    this.dashboardService = new DashboardService();
    this.log = new Logger();
    this.dashboard = {
      totalAccounts: 0, totalCustomers: 0, totalTransactions: 0
    }
    this.state = {
      dashboard: this.dashboard
    }
    this.initialize();
  }

  initialize() {
    this.dashboardService.getDashboards()
    .then((res: IDashboard) => {
      if(res !== undefined){
        this.dashboard = res;
        this.setState({dashboard: res}, () => this.render());
      }
    })
    .catch(error => this.log.error(error.message));
  }

  render() {
    return (
        <div className="container">
          <div className="jumbotron pt-2">
            <h2>ModularBank Dashboard</h2>
            <div>
              <img src="bank.png" alt="bank-logo" className="w-25 m-2"/>
            </div>
            <div className="card-deck mb-3 text-center mt-5">
              <div className="card mb-4 shadow-sm">
                <div className="card-header">
                  <h4 className="my-0 font-weight-normal">Total customers</h4>
                </div>
                <div className="card-body">
                  <h1 className="card-title pricing-card-title">{this.dashboard.totalAccounts}</h1>
                </div>
              </div>
              <div className="card mb-4 shadow-sm">
                <div className="card-header">
                  <h4 className="my-0 font-weight-normal">Total accounts</h4>
                </div>
                <div className="card-body">
                  <h1 className="card-title pricing-card-title">{this.dashboard.totalAccounts}</h1>
                </div>
              </div>
              <div className="card mb-4 shadow-sm">
                <div className="card-header">
                  <h4 className="my-0 font-weight-normal">Total transactions</h4>
                </div>
                <div className="card-body">
                  <h1 className="card-title pricing-card-title">{this.dashboard.totalTransactions}</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
  }
}