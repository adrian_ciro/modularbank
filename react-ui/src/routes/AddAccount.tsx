import React from "react";
import {Logger} from "../services/Logger";
import {AccountService} from "../services/AccountService";
import {IAccountResource} from "../models/api/IAccountResource";
import {IAccount} from "../models/IAccount";

export class AddAccount extends React.Component {

  log: Logger;
  accountService: AccountService;
  account: IAccountResource;
  isSaved: boolean;
  hasError: boolean;

  constructor(props: any) {
    super(props);
    this.accountService = new AccountService();
    this.log = new Logger();
    this.isSaved = false;
    this.hasError = false;
    this.account = {
      country: null, currency: [], customerId: null
    };
    this.state = {
      isSaved: this.isSaved,
      hasError: this.hasError,
      account: this.account
    };
  }

  save() {
    if(this.accountService.isValidAccount(this.account)){
      this.accountService.saveAccount(this.account)
      .then((res: IAccount[]) => {
        if(res !== undefined){
          this.hasError = false;
          this.isSaved = true;
          this.setState({isSaved: true, hasError: false}, () => this.render())
          this.log.info("Account saved" + res);
        }
        else{
          this.hasError = true;
          this.isSaved = false;
          this.setState({isSaved: false, hasError: true}, () => this.render())
          this.log.info("Account not saved");
        }
      })
      .catch(error => this.log.error(error.message));
    }
  }

  onChangeInput(e: React.FormEvent<HTMLInputElement>) {
    if (e.currentTarget.id === 'country') {
      this.account.country = e.currentTarget.value;
    } else if (e.currentTarget.id === 'customerId') {
      this.account.customerId = Number(e.currentTarget.value);
    }
    this.setState({account: this.account});
  }

  onChangeSelection(e: React.FormEvent<HTMLSelectElement>) {
    if (e.currentTarget.id === 'currencies') {
      this.account.currency = [];
      let array = Array.prototype.slice.call(e.currentTarget.children);
      array.forEach(node => this.updateSelectedCurrencies(node, this.account.currency));
    }
    this.setState({account: this.account});
  }

  updateSelectedCurrencies(node: HTMLOptionElement, currencies: Array<String>){
    if(node.selected){
      currencies.push(node.value);
    }
  }

  render() {
    return (
        <div className="container">
          <div className="jumbotron pt-2">
            <h2>Create Account</h2>

            <div className="mt-5">
              <form className="pl-lg-5 pr-lg-5 App-form text-center">
                <div className="form-group row">
                  <label htmlFor="country" className="col-sm-4 col-form-label text-right">Country</label>
                  <div className="col-sm-8">
                    <input type="text" pattern="[A-Za-z]+" className="form-control" id="country" required
                           onChange={(e) => this.onChangeInput(e)}/>
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="customerId" className="col-sm-4 col-form-label text-right">Customer Id</label>
                  <div className="col-sm-8">
                    <input type="number" className="form-control" id="customerId" required
                           onChange={(e) => this.onChangeInput(e)}/>
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="currencies" className="col-sm-4 col-form-label text-right">Currencies</label>
                  <div className="col-sm-8">
                    <select multiple id="currencies" className="form-control" required onChange={(e) => this.onChangeSelection(e)}>
                      <option>EUR</option>
                      <option>USD</option>
                      <option>GBP</option>
                      <option>SEK</option>
                    </select>
                  </div>
                </div>
                <div className="mt-5">
                  <button className="btn btn-primary pr-5 pl-5" onClick={() => this.save()}>Create account</button>
                </div>
                <div className="mt-5 alert alert-success" role="alert" hidden={!this.isSaved}>
                  The account has been created!
                </div>
                <div className="mt-5 alert alert-danger" role="alert" hidden={!this.hasError}>
                  The account was not created. Try again!
                </div>
              </form>
            </div>

          </div>
        </div>
    )
  }
}