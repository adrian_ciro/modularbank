import {IDashboard} from "../models/IDashboard";
import {ApiClient} from "./ApiClient";

export class DashboardService {

  api: ApiClient;

  constructor() {
    this.api = new ApiClient();
  }

  public async getDashboards(): Promise<IDashboard> {
    return await this.api.getDashboard();
  }

}
