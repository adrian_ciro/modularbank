import {Logger} from "./Logger";
import {IDashboard} from "../models/IDashboard";
import {IAccount} from "../models/IAccount";
import {ITransaction} from "../models/ITransaction";
import {IAccountResource} from "../models/api/IAccountResource";
import {ITransactionResource} from "../models/api/ITransactionResource";

export class ApiClient {

  baseUrl: string = 'http://localhost:8080/';
  dashboardPath: string = this.baseUrl + 'dashboards/';
  accountsPath: string = this.baseUrl + 'accounts/';
  transactionsPath: string = this.baseUrl + 'transactions/';
  log: Logger;

  constructor() {
    this.log = new Logger();
  }

  public getAccounts(): Promise<IAccount[]> {
    return this.get<IAccount[]>(this.accountsPath);
  }

  public getAccountsByCustomer(customerId: number): Promise<IAccount[]> {
    return this.get<IAccount[]>(this.accountsPath + customerId);
  }

  public postAccount(account: IAccountResource): Promise<IAccount[]> {
    return this.post<IAccount[]>(this.accountsPath, account);
  }

  public getTransactions(accountId: number): Promise<ITransaction[]> {
    return this.get<ITransaction[]>(this.transactionsPath + accountId);
  }

  public getDashboard(): Promise<IDashboard> {
    return this.get<IDashboard>(this.dashboardPath);
  }

  public postTransaction(transaction: ITransactionResource): Promise<ITransaction> {
    return this.post<ITransaction>(this.transactionsPath, transaction);
  }

  private async get<T>(path: string, args: RequestInit = {method: "get"}): Promise<T> {
    return await this.http<T>(new Request(path, args));
  };

  private async post<T>(path: string, body: any, args: RequestInit = {
    method: "POST",
    body: JSON.stringify(body),
    headers: {'Content-Type': 'application/json'},
  }): Promise<T> {
    return await this.http<T>(new Request(path, args));
  };

  private async http<T>(request: RequestInfo): Promise<T> {
    return await fetch(request)
    .then(res => {
      if (!(res.ok || res.status === 201)) {
        throw new Error("Got wrong http status")
      }
      return res.json()
    })
    .catch(err => this.log.error('Error fetching data! ' + err));
  }

}

