import {IAccountResource} from "../models/api/IAccountResource";
import {IAccount} from "../models/IAccount";
import {ApiClient} from "./ApiClient";

export class AccountService {

  api: ApiClient;

  constructor() {
    this.api = new ApiClient();
  }

  public isValidAccount(account:IAccountResource) : Boolean {
    return !(!account.country || !account.customerId || account.currency.length === 0);
  }

  public async getAllAccounts(): Promise<IAccount[]> {
    return await this.api.getAccounts();
  }

  public async getAccountsByCustomer(customerId:number): Promise<IAccount[]> {
    return await this.api.getAccountsByCustomer(customerId);
  }

  public async saveAccount(account:IAccountResource): Promise<IAccount[]> {
    return await this.api.postAccount(account);
  }
}
