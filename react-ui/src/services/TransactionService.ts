import {ITransactionResource} from "../models/api/ITransactionResource";
import {ITransaction} from "../models/ITransaction";
import {ApiClient} from "./ApiClient";

export class TransactionService {

  api: ApiClient;

  constructor() {
    this.api = new ApiClient();
  }

  public isValidTransaction(transaction:ITransactionResource) : Boolean {
    return !(!transaction.accountId || !transaction.amount || !transaction.currency || !transaction.direction || !transaction.description);
  }

  public async getTransactions(accountId: number): Promise<ITransaction[]> {
    return await this.api.getTransactions(accountId);
  }

  public async saveTransaction(transaction:ITransactionResource): Promise<ITransaction> {
    return await this.api.postTransaction(transaction);
  }
}
