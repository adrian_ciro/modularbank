import React from 'react';
import {HashRouter, Route, Switch} from 'react-router-dom';
import {Menu} from './components/Menu'
import {Home} from './routes/Home'
import {Accounts} from './routes/Accounts'
import {AccountDetails} from './routes/AccountDetails'
import {AddAccount} from './routes/AddAccount'
import {AddTransaction} from './routes/AddTransaction'
import {Transactions} from './routes/Transactions'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
      <div className="App">
        <HashRouter>
          <Menu/>
          <Switch>
            <Route exact={true} path="/" component={Home}/>
            <Route path="/accounts" component={Accounts}/>
            <Route path="/account-details/:id" component={AccountDetails}/>
            <Route path="/transactions/:id" component={Transactions}/>
            <Route path="/add-account" component={AddAccount}/>
            <Route path="/add-transaction" component={AddTransaction}/>
          </Switch>
        </HashRouter>
      </div>
  );
}

export default App;
