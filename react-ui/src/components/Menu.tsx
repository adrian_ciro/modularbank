import React from "react";
import {Link} from "react-router-dom";
import {CartFill, DiamondFill, HouseDoorFill, LayersFill} from 'react-bootstrap-icons';
import './components.css'

export class Menu extends React.Component {
  render() {
    return (
        <ul className="nav nav-pills nav-justified navbar-dark bg-dark p-3 mb-4">
          <li className="nav-item">
            <Link to="/"><HouseDoorFill className="menu-icon"/>Home</Link>
          </li>
          <li className="nav-item">
            <Link to="/accounts"><LayersFill className="menu-icon"/>Accounts</Link>
          </li>
          <li className="nav-item">
            <Link to="/add-account"><DiamondFill className="menu-icon"/>Create account</Link>
          </li>
          <li className="nav-item">
            <Link to="/add-transaction"><CartFill className="menu-icon"/>Create transaction</Link>
          </li>
        </ul>
    );
  }
}