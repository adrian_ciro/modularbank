export interface IAccountResource {
  customerId: number | null;
  country: string | null;
  currency: Array<String>;
}