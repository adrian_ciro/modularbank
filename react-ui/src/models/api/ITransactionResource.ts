export interface ITransactionResource {
  accountId: number | null;
  amount: number | null;
  currency: string | null;
  direction: string | null;
  description: string| null;
}