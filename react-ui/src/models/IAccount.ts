export interface IAccount {
  id: number;
  customerId: number;
  amount: number;
  currency: string;
  country: string;
}