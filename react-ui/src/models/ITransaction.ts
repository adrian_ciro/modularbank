export interface ITransaction {
  id : number;
  accountId: number;
  amount: number;
  currency: string;
  direction: string;
  description: string;
  balanceAfter: string;
}