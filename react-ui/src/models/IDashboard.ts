export interface IDashboard {
  totalCustomers: number;
  totalAccounts: number;
  totalTransactions: number;
}