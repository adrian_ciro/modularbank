# REACT APP FOR MODULARBANK 

### Description 
Frontend solution for modularbank backend systems. 
- It handles the creation of accounts and transactions.
- It has 3 general view to see all accounts, accounts per customer and transactions per account.
- General information will be shown in home page. 
- At account creation page, if you select several currencies, it will create several accounts (EUR, SEK, GBP, USD).


### Preconditions to run the app
- Nodejs 14 (12 and 13 also compatible, 15 not yet)
- Docker should be running with serice account up for interaction with backend.  


### How to run the application independently

Inside the directory react-ui, you can run via console:

`npm start`

Runs the app in the development mode.\
Open http://localhost:3000 to view it in the browser.

`npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.


### Remarks 
- It was my first time using react, I am more used to Angular but at the end no big differences. I enjoyed the challenge.
- A dashboard in the home page can be found as a user-friendliness feature.
- There is room for improvement, the html used in the routes can be extracted in components, and the use of JS libraries can help to reduce more code.  
- No tests can be found. I spent more time doing basic error handling and better user-friendly GUI's.