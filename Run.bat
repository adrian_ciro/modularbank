cd account
echo Building Account app
gradlew.bat build
cd ../report
echo Building Report app
gradlew.bat build
cd ../
echo Starting Docker containers
docker-compose build
docker-compose up -d