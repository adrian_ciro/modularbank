# MODULAR BANK 

This is a Full stack app used for a test assignment. Please install Docker-compose, otherwise the solution is not gonna work!

<br>

![Alt text](app.png?raw=true "App")


### Versioning

|            | Versions |
|------------|----------|
| Java       | 11       |
| Springboot | 2.3.6    |
| Docker     |          |   
| React      | 17       |
| Node       |          |


### How to run the application
For unix, open the terminal and go to the root folder and type

```shell
./Run.sh
```

Open http://localhost

For accessing account database see credentials [here](account/README.md)\
For accessing report database see credentials [here](report/README.md)


### Considerations to scale applications

Nowadays, we have new approaches like Kubernetes, Docker Swarm or serverless. This new technologies allow the configuration 
of rules that creates replicas of the containers automatically based on current load of data and this is supported for the main cloud computing services like AWS, Microsoft azure and Google cloud.
Now, in this particular solution we can achieve horizontal scalability with the command 

`docker-compose up -d --scale account=3`

However, this requires small tweaks in the file _docker-compose.yml_ for every service. First, removing property _container_name_ and after, 
configuring the _ports_ to a range of possible values like 

`"8080-8083:8080" `
