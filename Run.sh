#!/bin/sh

cd account
echo Building Account app
./gradlew build
cd ../report
echo Building Report app
./gradlew build
cd ../
echo Starting Docker containers
docker-compose build
docker-compose up -d