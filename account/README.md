# ACCOUNT MICRO-SERVICE 

### Description 
Microservice Account keeps track of accounts, their balances and transactions. A customer can have 
max 4 accounts depending on the allowed currencies (EUR, SEK, GBP, USD).
For every creation of an account or an update, a msg is publish in RabbitMQ.
For every creation of a transaction, a msg is publish in RabbitMQ.


### Preconditions to run the app
- Installed Java 11
- Docker should be running with database postgres and rabbit containers running  


### How to run the application independently
Be sure you are in folder account and build the project

For windows\
`gradlew.bat build`

For unix\
`./gradlew build`

Then start up the application

For windows\
`gradlew.bat bootRun`

For unix\
`./gradlew bootRun`


### Swagger url
In order to verify the microservice is up and running, go to\
`http://localhost:8080/swagger-ui/`


### Database
In order to verify the DB is saving properly the information, please connect using an DB visualizer like DBeaver or DataGrip\
`jdbc:postgresql://localhost:8832/postgres`
user: postgres
pass: easypassword


### Remarks
- The solution has the typical pattern of controllers (presentation layer), services (business rules) and model (data).
- A dashboard rest endpoint was added as enhancement for the gui. 
- One account and one transaction are created as initial data every time the app is started.
- Mapstruct library is used in the presentation layer to converter from domain objects to resources.
- Custom error handling is implemented. See ErrorHandlerConfig class.
- Swagger library is used in order to have a better overview of all endpoints.
- Code coverage of tests is more than 80%.