package co.modularbank.account.controller;

import static org.springframework.http.ResponseEntity.ok;

import co.modularbank.account.controller.resource.DashboardResource;
import co.modularbank.account.service.AccountService;
import co.modularbank.account.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/dashboards")
public class DashboardRestController {

  private final AccountService accountService;
  private final TransactionService transactionService;

  @GetMapping
  public ResponseEntity<DashboardResource> getDashboardSummary() {
    Integer totalCustomers = accountService.getTotalCustomers();
    Integer totalAccounts = accountService.getTotalAccounts();
    Integer totalTransactions = transactionService.getTotalTransactions();
    return ok(new DashboardResource(totalCustomers, totalAccounts, totalTransactions));
  }

}
