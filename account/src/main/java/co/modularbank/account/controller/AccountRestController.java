package co.modularbank.account.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

import co.modularbank.account.controller.resource.AccountInputResource;
import co.modularbank.account.controller.resource.AccountResource;
import co.modularbank.account.controller.resource.ResourceConverter;
import co.modularbank.account.dao.Account;
import co.modularbank.account.service.AccountService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/accounts")
public class AccountRestController {

  private final AccountService accountService;
  private final ResourceConverter converter;

  @GetMapping
  public ResponseEntity<List<AccountResource>> getAccounts() {
    List<Account> accounts = accountService.getAllAccounts();
    return ok(converter.toAccountList(accounts));
  }

  @GetMapping("/{customerId}")
  public ResponseEntity<List<AccountResource>> getAccountsByCustomer(@PathVariable Long customerId) {
    List<Account> customerAccounts = accountService.getAccountsByCustomer(customerId);
    return customerAccounts.isEmpty() ? notFound().build() : ok(converter.toAccountList(customerAccounts));
  }

  @PostMapping
  public ResponseEntity<List<AccountResource>> createAccounts(@Valid @RequestBody AccountInputResource inputResource) {
    List<Account> accounts = accountService
        .createAccounts(inputResource.getCustomerId(), inputResource.getCountry(), inputResource.getCurrency());
    return status(CREATED).body(converter.toAccountList(accounts));
  }

}
