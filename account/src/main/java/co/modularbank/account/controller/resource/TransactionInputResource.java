package co.modularbank.account.controller.resource;

import co.modularbank.account.dao.enums.Currency;
import co.modularbank.account.dao.enums.TransactionDirection;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TransactionInputResource {

  @NotNull(message = "AccountId is empty")
  Long accountId;
  @NotNull(message = "Amount is empty")
  @Positive(message = "Amount should be positive")
  BigDecimal amount;
  @NotNull(message = "Currency is missing or currency is not allowed")
  Currency currency;
  @NotNull
  TransactionDirection direction;
  @NotBlank(message = "Description is empty")
  String description;

}
