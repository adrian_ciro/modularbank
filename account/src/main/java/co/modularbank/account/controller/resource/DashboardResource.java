package co.modularbank.account.controller.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DashboardResource {

  Integer totalCustomers;
  Integer totalAccounts;
  Integer totalTransactions;

}
