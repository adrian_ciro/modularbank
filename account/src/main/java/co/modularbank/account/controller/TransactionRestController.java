package co.modularbank.account.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

import co.modularbank.account.controller.resource.ResourceConverter;
import co.modularbank.account.controller.resource.TransactionInputResource;
import co.modularbank.account.controller.resource.TransactionResource;
import co.modularbank.account.dao.Transaction;
import co.modularbank.account.service.TransactionService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/transactions")
public class TransactionRestController {

  private final TransactionService transactionService;
  private final ResourceConverter converter;

  @GetMapping("/{accountId}")
  private ResponseEntity<List<TransactionResource>> getTransactionsByAccount(@PathVariable Long accountId) {
    List<Transaction> transactionsByAccount = transactionService.getTransactionsByAccount(accountId);
    return ok().body(converter.toTransactionResourceList(transactionsByAccount));
  }

  @PostMapping
  private ResponseEntity<TransactionResource> createTransaction(@Valid @RequestBody TransactionInputResource inputResource) {
    Transaction transaction = transactionService.createTransaction(converter.toTransaction(inputResource));
    return status(CREATED).body(converter.toTransactionResource(transaction));
  }

}
