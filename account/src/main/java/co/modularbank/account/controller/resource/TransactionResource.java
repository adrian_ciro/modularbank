package co.modularbank.account.controller.resource;

import co.modularbank.account.dao.enums.Currency;
import co.modularbank.account.dao.enums.TransactionDirection;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TransactionResource {

  Long id;
  Long accountId;
  BigDecimal amount;
  Currency currency;
  TransactionDirection direction;
  String description;
  BigDecimal currentBalance;

}
