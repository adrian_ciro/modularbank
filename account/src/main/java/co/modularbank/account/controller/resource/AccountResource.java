package co.modularbank.account.controller.resource;

import co.modularbank.account.dao.enums.Currency;
import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AccountResource {

  Long id;
  Long customerId;
  BigDecimal amount;
  Currency currency;
  String country;

}
