package co.modularbank.account.controller.resource;

import static org.mapstruct.ReportingPolicy.IGNORE;

import co.modularbank.account.dao.Account;
import co.modularbank.account.dao.Transaction;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface ResourceConverter {

  Transaction toTransaction(TransactionInputResource resource);

  @Mapping(target = "currentBalance", source = "balanceAfter")
  TransactionResource toTransactionResource(Transaction resource);

  List<TransactionResource> toTransactionResourceList(List<Transaction> resources);

  List<AccountResource> toAccountList(List<Account> accounts);
}
