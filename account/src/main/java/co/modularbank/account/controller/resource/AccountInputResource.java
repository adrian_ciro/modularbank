package co.modularbank.account.controller.resource;

import co.modularbank.account.dao.enums.Currency;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AccountInputResource {

  @NotNull
  Long customerId;
  @NotNull
  String country;
  @NotNull
  @Size(min = 1, message = "Currency is missing or it is not allowed")
  List<Currency> currency;

}
