package co.modularbank.account.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitSenderService {

  private static final String EXCHANGE_TOPIC = "co.modularbank";
  private static final String ROUTING_KEY_ACCOUNT_CREATED = "account.created";
  private static final String ROUTING_KEY_ACCOUNT_UPDATED = "account.updated";
  private static final String ROUTING_KEY_TRANSACTION_CREATED = "transaction.created";

  private final RabbitTemplate rabbitTemplate;
  private final ObjectMapper objectMapper;

  public void sendMsgAccountCreated(Object object) {
    sendMsg(EXCHANGE_TOPIC, ROUTING_KEY_ACCOUNT_CREATED, object);
  }

  public void sendMsgAccountUpdated(Object object) {
    sendMsg(EXCHANGE_TOPIC, ROUTING_KEY_ACCOUNT_UPDATED, object);
  }

  public void sendMsgTransactionCreated(Object object) {
    sendMsg(EXCHANGE_TOPIC, ROUTING_KEY_TRANSACTION_CREATED, object);
  }

  private void sendMsg(String exchange, String routingKey, Object object) {
    try {
      rabbitTemplate.convertAndSend(exchange, routingKey, objectMapper.writeValueAsString(object));
    } catch (Exception e) {
      log.error("Error sending message to RabbitMQ");
    }
  }

}
