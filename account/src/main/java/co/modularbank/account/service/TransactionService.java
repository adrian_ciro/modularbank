package co.modularbank.account.service;

import static co.modularbank.account.dao.enums.TransactionDirection.IN;
import static co.modularbank.account.dao.enums.TransactionDirection.OUT;
import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import co.modularbank.account.dao.Account;
import co.modularbank.account.dao.Transaction;
import co.modularbank.account.dao.TransactionDaoMapper;
import co.modularbank.account.dao.enums.Currency;
import java.math.BigDecimal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpClientErrorException.NotFound;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionService {

  private final TransactionDaoMapper transactionDaoMapper;
  private final AccountService accountService;
  private final RabbitSenderService rabbitSenderService;

  public List<Transaction> getTransactionsByAccount(Long accountId) {
    Account account = accountService.getAccountsById(accountId);
    validateAccount(account);
    return transactionDaoMapper.findByAccountId(accountId);
  }

  public Transaction createTransaction(Transaction transaction) {
    Account account = accountService.getAccountsById(transaction.getAccountId());
    validateTransaction(account, transaction);
    return saveTransaction(transaction, account);
  }

  public Integer getTotalTransactions() {
    return transactionDaoMapper.getTotalTransactions();
  }

  private Transaction saveTransaction(Transaction transaction, Account account) {
    BigDecimal newBalance = getNewBalance(transaction, account);
    account.setAmount(newBalance);
    transaction.setBalanceAfter(newBalance);
    try {
      transactionDaoMapper.save(transaction);
      rabbitSenderService.sendMsgTransactionCreated(transaction);
      accountService.updateAccount(account);
    } catch (Exception e) {
      log.error("Error saving transaction for account " + transaction.getAccountId());
      throw new RuntimeException("Error while inserting transaction in database");
    }
    return transaction;
  }

  private BigDecimal getNewBalance(Transaction transaction, Account account) {
    return transaction.getDirection() == IN ? account.getAmount().add(transaction.getAmount())
        : account.getAmount().subtract(transaction.getAmount());
  }

  private void validateTransaction(Account account, Transaction transaction) {
    validateAccount(account);
    validateCurrency(account.getCurrency(), transaction.getCurrency());
    validateFunds(account.getAmount(), transaction);
  }

  private void validateFunds(BigDecimal balance, Transaction transaction) {
    if (transaction.getDirection() == OUT && balance.compareTo(transaction.getAmount()) < 0) {
      throw new HttpClientErrorException(METHOD_NOT_ALLOWED, "Can not create transaction. Insufficient amount.");
    }
  }

  private void validateCurrency(Currency accountCurrency, Currency transactionCurrency) {
    if (accountCurrency != transactionCurrency) {
      throw new HttpClientErrorException(BAD_REQUEST,
          "Can not create transaction. Different types of currencies between account and transaction");
    }
  }

  private void validateAccount(Account account) throws NotFound {
    if (isNull(account)) {
      throw new HttpClientErrorException(NOT_FOUND, "Account not found");
    }
  }
}
