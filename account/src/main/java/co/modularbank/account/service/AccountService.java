package co.modularbank.account.service;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import co.modularbank.account.dao.Account;
import co.modularbank.account.dao.AccountDaoMapper;
import co.modularbank.account.dao.enums.Currency;
import java.math.BigDecimal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

  private final AccountDaoMapper accountDaoMapper;
  private final RabbitSenderService rabbitSenderService;

  public Integer getTotalCustomers() {
    return accountDaoMapper.findTotalCustomers();
  }

  public List<Account> getAllAccounts() {
    return accountDaoMapper.findAll();
  }

  public Integer getTotalAccounts() {
    return accountDaoMapper.getTotalAccounts();
  }

  public List<Account> getAccountsByCustomer(Long customerId) {
    return accountDaoMapper.findByCustomerId(customerId);
  }

  public Account getAccountsById(Long id) {
    return accountDaoMapper.findById(id);
  }

  public List<Account> createAccounts(Long customerId, String country, List<Currency> currencies) {
    String[] currenciesArray = currencies.stream().map(Currency::name).toArray(String[]::new);
    validateExistingCurrencies(customerId, currenciesArray);
    currencies.forEach(currency -> saveAccount(customerId, country, currency));
    return accountDaoMapper.findByCustomerIdAndCurrencies(customerId, currenciesArray);
  }

  public void updateAccount(Account account) {
    try {
      accountDaoMapper.updateAmount(account);
      rabbitSenderService.sendMsgAccountUpdated(account);
    } catch (Exception e) {
      log.error("Error updating account " + account.getId());
      throw new RuntimeException("Error while inserting account in database");
    }
  }

  private void saveAccount(Long customerId, String country, Currency currency) {
    Account account = initializeAccount(customerId, country, currency);
    try {
      accountDaoMapper.save(account);
      rabbitSenderService.sendMsgAccountCreated(account);
    } catch (Exception e) {
      log.error("Error saving account for customer " + customerId);
      throw new RuntimeException("Error while inserting account in database");
    }
  }

  private void validateExistingCurrencies(Long customerId, String[] currencies) {
    boolean existsAnyAccount = accountDaoMapper.findByCustomerIdAndCurrencies(customerId, currencies).size() > 0;
    if (existsAnyAccount) {
      throw new HttpClientErrorException(BAD_REQUEST, "Can not create account. Existing currency already exist for customer " + customerId);
    }
  }

  private Account initializeAccount(Long customerId, String country, Currency currency) {
    return Account.builder()
        .amount(BigDecimal.ZERO)
        .currency(currency)
        .country(country)
        .customerId(customerId)
        .build();
  }
}
