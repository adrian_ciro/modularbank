package co.modularbank.account.config;

import static java.util.Objects.nonNull;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ErrorHandlerConfig extends ResponseEntityExceptionHandler {

  public static final String UNKNOWN_ERROR = "unknown_error";
  public static final String INTERNAL_ERROR = "internal_server_error";
  public static final String INVALID_METHOD_ARGUMENT_ERROR = "invalid_method_argument";

  @ExceptionHandler(HttpStatusCodeException.class)
  public ResponseEntity handleHttpStatusCodeException(HttpStatusCodeException exception) {
    String errorCode = exception.getStatusText();
    return createApiError(exception.getMessage(), exception.getStatusCode(), nonNull(errorCode) ? errorCode : UNKNOWN_ERROR);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException exception) {
    String message = exception.getName() + ": invalid value" + " (" + exception.getValue() + ")";
    return createApiError(message, BAD_REQUEST, INVALID_METHOD_ARGUMENT_ERROR);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity handleAll(Exception exception) {
    return createApiError(exception.getMessage(), INTERNAL_SERVER_ERROR, INTERNAL_ERROR);
  }

  @Override
  @SuppressWarnings("unchecked")
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException exception,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    String message = exception.getParameterName() + " parameter is missing";
    return createApiError(message, BAD_REQUEST, "missing_request_parameter");
  }

  @Override
  @SuppressWarnings("unchecked")
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException exception, HttpHeaders headers,
      HttpStatus status, WebRequest request) {
    String message = exception.getCause().getMessage();
    String code = UNKNOWN_ERROR;

    if (exception.getCause() instanceof InvalidFormatException) {
      message = ((InvalidFormatException) exception.getCause()).getOriginalMessage();
      code = "invalid_format";
    } else if (exception.getCause() instanceof MismatchedInputException) {
      message = ((MismatchedInputException) exception.getCause()).getOriginalMessage();
      code = "mismatched_input";
    } else if (exception.getCause() instanceof JsonMappingException) {
      message = parseMessage(((JsonMappingException) exception.getCause()).getOriginalMessage());
      code = "unable_to_map_json";
    } else if (exception.getCause() instanceof JsonParseException) {
      message = parseMessage(((JsonParseException) exception.getCause()).getOriginalMessage());
      code = "unable_to_parse_json";
    }
    log.error(exception.getMessage(), exception);
    return createApiError(message, status, code);
  }

  @Override
  @SuppressWarnings("unchecked")
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    BindingResult bindingResult = exception.getBindingResult();
    FieldError fieldError = null;
    String message = null;

    if (bindingResult != null) {
      fieldError = bindingResult.getFieldError();
    }
    if (fieldError != null) {
      message = fieldError.getField() + ": " + fieldError.getDefaultMessage();
    }
    log.error(exception.getMessage(), exception);
    return createApiError(message, status, INVALID_METHOD_ARGUMENT_ERROR);
  }

  @SuppressWarnings("unchecked")
  public ResponseEntity createApiError(String message, HttpStatus httpStatus, String code) {
    log.error(message);
    ApiError apiError = new ApiError(httpStatus, message, code);
    return new ResponseEntity(apiError, new HttpHeaders(), httpStatus);
  }

  private String parseMessage(String message) {
    return message
        .replaceAll("was expecting ", "")
        .replaceAll(": expected a valid value", "")
        .replaceAll("number, String, array, object, 'true', 'false' or 'null'", "expected a valid value")
        .replaceAll("'true', 'false' or 'null'", "expected a valid value")
        .replaceAll("null', 'true', 'false' or NaN", "expected a valid value");
  }

  @Value
  @AllArgsConstructor
  public class ApiError {

    HttpStatus status;
    String message;
    String errorCode;
  }
}
