package co.modularbank.account.dao.enums;

public enum Currency {
  EUR,
  SEK,
  GBP,
  USD
}
