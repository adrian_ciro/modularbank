package co.modularbank.account.dao;

import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TransactionDaoMapper {

  @Select("select * from transaction where id = #{id}")
  Transaction findById(@Param("id") Long id);

  @Select("select * from transaction where accountId = #{accountId}")
  List<Transaction> findByAccountId(@Param("accountId") Long accountId);

  @Insert("insert into transaction (accountId, amount, currency, description, balanceAfter, direction) VALUES "
      + "(#{transaction.accountId},#{transaction.amount},#{transaction.currency},#{transaction.description}, "
      + "#{transaction.balanceAfter},#{transaction.direction})")
  @Options(useGeneratedKeys = true, keyProperty = "id")
  void save(@Param("transaction") Transaction transaction) throws Exception;

  @Select("select count(*) from transaction")
  Integer getTotalTransactions();
}
