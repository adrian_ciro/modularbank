package co.modularbank.account.dao;

import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface AccountDaoMapper {

  @Select("select id, customerId, country, amount, currency from account where id = #{id}")
  Account findById(@Param("id") Long id);

  @Select("select * from account order by customerId")
  List<Account> findAll();

  @Select("select distinct count(customerId) from account")
  Integer findTotalCustomers();

  @Select("select count(*) from account")
  Integer getTotalAccounts();

  @Select("select * from account where customerId = #{customerId}")
  List<Account> findByCustomerId(@Param("customerId") Long customerId);

  @Select("select * from account where customerId = #{customerId} and currency = ANY(#{currencies})")
  List<Account> findByCustomerIdAndCurrencies(@Param("customerId") Long customerId, @Param("currencies") String[] currencies);

  @Insert("insert into account (customerId, country, amount, currency) VALUES "
      + "( #{account.customerId}, #{account.country},#{account.amount},#{account.currency})")
  @Options(useGeneratedKeys = true, keyProperty = "id")
  void save(@Param("account") Account account) throws Exception;

  @Update("update account set amount= #{account.amount} where id = #{account.id}")
  void updateAmount(@Param("account") Account account) throws Exception;
}
