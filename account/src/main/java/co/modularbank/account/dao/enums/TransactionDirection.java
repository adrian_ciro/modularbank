package co.modularbank.account.dao.enums;

public enum TransactionDirection {
  IN,
  OUT
}
