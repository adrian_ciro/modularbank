package co.modularbank.account.dao;

import co.modularbank.account.dao.enums.Currency;
import co.modularbank.account.dao.enums.TransactionDirection;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Transaction implements Serializable {

  private Long id;
  private Long accountId;
  private BigDecimal amount;
  private Currency currency;
  private TransactionDirection direction;
  private String description;
  private BigDecimal balanceAfter;

}
