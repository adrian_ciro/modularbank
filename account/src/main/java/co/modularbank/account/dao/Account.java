package co.modularbank.account.dao;

import co.modularbank.account.dao.enums.Currency;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Account implements Serializable {

  private Long id;
  private Long customerId;
  private String country;
  private BigDecimal amount;
  private Currency currency;

}
