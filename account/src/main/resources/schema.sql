drop table if exists account;
drop table if exists transaction;

create table account
(
    id         bigserial primary key,
    customerId int,
    country    varchar,
    amount     numeric(16, 2),
    currency   varchar
);

create table transaction
(
    id           bigserial primary key,
    accountId    int,
    amount       numeric(16, 2),
    currency     varchar,
    direction    varchar,
    description  varchar,
    balanceAfter numeric(16, 2)
);

