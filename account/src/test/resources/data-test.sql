insert into account (customerId, country, amount, currency) values ( 1, 'US', 100, 'USD');
insert into account (customerId, country, amount, currency) values ( 2, 'EE', 200, 'EUR');

insert into account (customerId, country, amount, currency) values ( 3, 'EE', 200, 'EUR');
insert into transaction (accountId, amount, currency, direction, description, balanceAfter) values ( 3, 10, 'EUR', 'OUT', 'Coffee', 190);
