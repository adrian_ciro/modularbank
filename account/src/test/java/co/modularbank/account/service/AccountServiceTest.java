package co.modularbank.account.service;

import static co.modularbank.account.dao.enums.Currency.EUR;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import co.modularbank.account.base.BaseUnitTest;
import co.modularbank.account.dao.Account;
import co.modularbank.account.dao.AccountDaoMapper;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.web.client.HttpClientErrorException;

class AccountServiceTest extends BaseUnitTest {

  private static final long CUSTOMER_ID = 1L;

  @InjectMocks
  AccountService accountService;
  @Mock
  AccountDaoMapper accountDaoMapper;

  @Test
  void createAccountsAndThrowsException() throws Exception {
    Account account = getAccount();
    String[] currencies = {"EUR"};
    when(accountDaoMapper.findByCustomerIdAndCurrencies(CUSTOMER_ID, currencies)).thenReturn(emptyList());
    doThrow(new RuntimeException()).when(accountDaoMapper).save(account);

    assertThrows(Exception.class, () -> accountService.createAccounts(CUSTOMER_ID, "EE", List.of(EUR)));
  }

  @Test
  void createAccountsAndThrowsHttpClientErrorExceptionWhenCurrencyExist() {
    Account account = getAccount();
    String[] currencies = {"EUR"};
    when(accountDaoMapper.findByCustomerIdAndCurrencies(CUSTOMER_ID, currencies)).thenReturn(List.of(account));

    assertThrows(HttpClientErrorException.class, () -> accountService.createAccounts(CUSTOMER_ID, "EE", List.of(EUR)));
  }

  @Test
  void updateAccountAndThrowsException() throws Exception {
    Account account = getAccount();
    doThrow(new RuntimeException()).when(accountDaoMapper).updateAmount(account);

    assertThrows(Exception.class, () -> accountService.updateAccount(account));
  }

  private Account getAccount() {
    return Account.builder()
        .customerId(CUSTOMER_ID)
        .amount(BigDecimal.ZERO)
        .country("EE")
        .currency(EUR)
        .build();
  }
}