package co.modularbank.account.base;

import static org.springframework.http.MediaType.APPLICATION_JSON;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@DirtiesContext
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public abstract class BaseIntTest {

  @Autowired
  protected MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;
  @MockBean
  private RabbitTemplate rabbitTemplate;

  protected MockHttpServletRequestBuilder getRequest(String url) {
    return MockMvcRequestBuilders.get(url)
        .contentType(APPLICATION_JSON);
  }

  protected MockHttpServletRequestBuilder postRequest(String url) {
    return MockMvcRequestBuilders.post(url)
        .contentType(APPLICATION_JSON)
        .accept(APPLICATION_JSON);
  }

  protected String toJson(Object object) throws JsonProcessingException {
    return objectMapper.writeValueAsString(object);
  }

  protected <T> T fromJson(MvcResult result, Class<T> objectClass) throws IOException {
    return (T) objectMapper.readValue(result.getResponse().getContentAsString(), objectClass);
  }

  protected <T> T fromJsonAsList(MvcResult result, TypeReference<T> type) throws IOException {
    return (T) objectMapper.readValue(result.getResponse().getContentAsString(), type);
  }

}