package co.modularbank.account.controller;

import static co.modularbank.account.dao.enums.Currency.EUR;
import static co.modularbank.account.dao.enums.Currency.SEK;
import static co.modularbank.account.dao.enums.Currency.USD;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import co.modularbank.account.base.BaseIntTest;
import co.modularbank.account.controller.resource.AccountInputResource;
import co.modularbank.account.controller.resource.AccountResource;
import co.modularbank.account.dao.enums.Currency;
import com.fasterxml.jackson.core.type.TypeReference;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.test.web.servlet.MvcResult;

@TestMethodOrder(OrderAnnotation.class)
class AccountRestControllerIntTest extends BaseIntTest {

  public static final String BASE_URL = "/accounts";

  @Test
  @Order(1)
  void getAllAccountsSuccessfully() throws Exception {
    MvcResult result = mockMvc.perform(getRequest(BASE_URL))
        .andExpect(status().isOk())
        .andReturn();

    List<AccountResource> resources = fromJsonAsList(result, new TypeReference<List<AccountResource>>() {});

    AccountResource account0 = getAccountResource(1L, "US", 1L, USD, BigDecimal.valueOf(100));
    AccountResource account1 = getAccountResource(2L, "EE", 2L, EUR, BigDecimal.valueOf(200));
    AccountResource account2 = getAccountResource(3L, "EE", 3L, EUR, BigDecimal.valueOf(200));

    assertThat(resources.size()).isEqualTo(3);
    assertAccounts(resources.get(0), account0);
    assertAccounts(resources.get(1), account1);
    assertAccounts(resources.get(2), account2);
  }

  @Test
  void getAccountsByCustomerSuccessfully() throws Exception {
    MvcResult result = mockMvc.perform(getRequest(BASE_URL + "/1"))
        .andExpect(status().isOk())
        .andReturn();

    List<AccountResource> resources = fromJsonAsList(result, new TypeReference<List<AccountResource>>() {});

    AccountResource account0 = getAccountResource(1L, "US", 1L, USD, BigDecimal.valueOf(100));

    assertThat(resources.size()).isEqualTo(1);
    assertAccounts(resources.get(0), account0);
  }

  @Test
  void getAccountsByCustomerWhenCustomerIsNotFound() throws Exception {
    mockMvc.perform(getRequest(BASE_URL + "/100"))
        .andExpect(status().isNotFound())
        .andReturn();
  }

  @Test
  void createAccountsSuccessfully() throws Exception {
    AccountInputResource inputResource = getInputResource(List.of(SEK));

    MvcResult result = mockMvc.perform(postRequest(BASE_URL)
        .content(toJson(inputResource)))
        .andExpect(status().isCreated())
        .andReturn();

    List<AccountResource> resources = fromJsonAsList(result, new TypeReference<List<AccountResource>>() {});

    AccountResource account = getAccountResource(4L, "AUS", 3L, SEK, BigDecimal.ZERO);

    assertThat(resources).isNotNull();
    assertAccounts(resources.get(0), account);
  }

  @Test
  void createAccountsReturnsBadRequestWhenCurrenciesAreEmpty() throws Exception {
    AccountInputResource inputResource = getInputResource(emptyList());

    mockMvc.perform(postRequest(BASE_URL)
        .content(toJson(inputResource)))
        .andExpect(status().isBadRequest())
        .andReturn();
  }

  private void assertAccounts(AccountResource actual, AccountResource expected) {
    assertThat(actual.getId()).isEqualTo(expected.getId());
    assertThat(actual.getCustomerId()).isEqualTo(expected.getCustomerId());
    assertThat(actual.getAmount()).isEqualByComparingTo(expected.getAmount());
    assertThat(actual.getCurrency()).isEqualTo(expected.getCurrency());
    assertThat(actual.getCountry()).isEqualTo(expected.getCountry());
  }

  private AccountInputResource getInputResource(List<Currency> currencies) {
    return AccountInputResource.builder()
        .country("AUS")
        .customerId(3L)
        .currency(currencies)
        .build();
  }

  private AccountResource getAccountResource(Long id, String country, Long customerId, Currency currency, BigDecimal amount) {
    return AccountResource.builder()
        .id(id)
        .country(country)
        .customerId(customerId)
        .currency(currency)
        .amount(amount)
        .build();
  }
}