package co.modularbank.account.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import co.modularbank.account.base.BaseIntTest;
import co.modularbank.account.controller.resource.DashboardResource;
import org.junit.jupiter.api.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MvcResult;

class DashboardRestControllerTest extends BaseIntTest {

  public static final String BASE_URL = "/dashboards";

  @Test
  void getDashboardSummary() throws Exception {

    MvcResult result = mockMvc.perform(getRequest(BASE_URL))
        .andExpect(status().isOk())
        .andReturn();

    DashboardResource resource = fromJson(result, DashboardResource.class);

    assertThat(resource.getTotalCustomers()).isEqualTo(3);
    assertThat(resource.getTotalAccounts()).isEqualTo(3);
    assertThat(resource.getTotalTransactions()).isEqualTo(1);
  }
}