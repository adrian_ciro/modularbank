package co.modularbank.account.controller;

import static co.modularbank.account.dao.enums.Currency.EUR;
import static co.modularbank.account.dao.enums.Currency.GBP;
import static co.modularbank.account.dao.enums.TransactionDirection.OUT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import co.modularbank.account.base.BaseIntTest;
import co.modularbank.account.controller.resource.TransactionInputResource;
import co.modularbank.account.controller.resource.TransactionResource;
import com.fasterxml.jackson.core.type.TypeReference;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.test.web.servlet.MvcResult;

@TestMethodOrder(OrderAnnotation.class)
class TransactionRestControllerIntTest extends BaseIntTest {

  public static final String BASE_URL = "/transactions";

  @Test
  @Order(1)
  void getAllTransactionsSuccessfully() throws Exception {
    MvcResult result = mockMvc.perform(getRequest(BASE_URL + "/3"))
        .andExpect(status().isOk())
        .andReturn();

    List<TransactionResource> transactions = fromJsonAsList(result, new TypeReference<List<TransactionResource>>() {
    });

    assertThat(transactions.size()).isEqualTo(1);
    assertThat(transactions.get(0).getId()).isEqualTo(1);
    assertThat(transactions.get(0).getAccountId()).isEqualTo(3);
    assertThat(transactions.get(0).getDescription()).isEqualTo("Coffee");
    assertThat(transactions.get(0).getCurrency()).isEqualTo(EUR);
    assertThat(transactions.get(0).getDirection()).isEqualTo(OUT);
    assertThat(transactions.get(0).getAmount()).isEqualByComparingTo(BigDecimal.valueOf(10));
    assertThat(transactions.get(0).getCurrentBalance()).isEqualByComparingTo(BigDecimal.valueOf(190));
  }

  @Test
  @Order(2)
  void getTransactionsWhenAccountDoesNotExist() throws Exception {
    mockMvc.perform(getRequest(BASE_URL + "/100"))
        .andExpect(status().isNotFound())
        .andReturn();
  }

  @Test
  void createTransactionReturnsBadRequestWhenInvalidAmount() throws Exception {
    TransactionInputResource inputResource = getInputResource(BigDecimal.TEN.negate(), true);

    mockMvc.perform(postRequest(BASE_URL)
        .content(toJson(inputResource)))
        .andExpect(status().isBadRequest())
        .andReturn();
  }

  @Test
  void createTransactionReturnsBadRequestWhenInsufficientFunds() throws Exception {
    TransactionInputResource inputResource = getInputResource(BigDecimal.valueOf(300), true);

    mockMvc.perform(postRequest(BASE_URL)
        .content(toJson(inputResource)))
        .andExpect(status().isMethodNotAllowed())
        .andReturn();
  }

  @Test
  void createTransactionReturnsBadRequestWhenCurrencyIsDifferentFromAccount() throws Exception {
    TransactionInputResource inputResource = getInputResource(BigDecimal.TEN, false);

    mockMvc.perform(postRequest(BASE_URL)
        .content(toJson(inputResource)))
        .andExpect(status().isBadRequest())
        .andReturn();
  }

  @Test
  @Order(3)
  void createTransactionSuccessfully() throws Exception {
    TransactionInputResource inputResource = getInputResource(BigDecimal.TEN, true);

    MvcResult result = mockMvc.perform(postRequest(BASE_URL)
        .content(toJson(inputResource)))
        .andExpect(status().isCreated())
        .andReturn();

    TransactionResource transaction = fromJson(result, TransactionResource.class);

    assertThat(transaction.getId()).isEqualTo(2);
    assertThat(transaction.getAccountId()).isEqualTo(3);
    assertThat(transaction.getDirection()).isEqualTo(OUT);
    assertThat(transaction.getCurrency()).isEqualTo(EUR);
    assertThat(transaction.getAmount()).isEqualTo(BigDecimal.TEN);
    assertThat(transaction.getCurrentBalance()).isEqualByComparingTo(BigDecimal.valueOf(190));
    assertThat(transaction.getDescription()).isEqualTo("Test");
  }

  private TransactionInputResource getInputResource(BigDecimal amount, boolean isEur) {
    return TransactionInputResource.builder()
        .accountId(3L)
        .amount(amount)
        .currency(isEur ? EUR : GBP)
        .direction(OUT)
        .description("Test")
        .build();
  }

}