package co.modularbank.account.controller.resource;

import static org.assertj.core.api.Assertions.assertThat;

import co.modularbank.account.base.BaseUnitTest;
import co.modularbank.account.dao.Account;
import co.modularbank.account.dao.Transaction;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ResourceConverterTest extends BaseUnitTest {

  private static ResourceConverterImpl converter;

  @BeforeAll
  static void setUp() {
    converter = new ResourceConverterImpl();
  }

  @Test
  void toAccountResourceWhenParamsAreNull() {
    List<AccountResource> resource = converter.toAccountList(null);

    assertThat(resource).isNull();
  }

  @Test
  void toAccountResourceWhenAccountIsNotNull() {
    AccountResource resource = converter.accountToAccountResource(null);

    assertThat(resource).isNull();
  }

  @Test
  void toTransactionWhenParamIsNull() {
    Transaction transaction = converter.toTransaction(null);
    assertThat(transaction).isNull();
  }

  @Test
  void toTransactionResourceWhenParamIsNull() {
    TransactionResource transactionResource = converter.toTransactionResource(null);

    assertThat(transactionResource).isNull();
  }

  @Test
  void toTransactionResourceListWhenParamIsNull() {
    List<TransactionResource> transactionResourceList = converter.toTransactionResourceList(null);

    assertThat(transactionResourceList).isNull();
  }
}