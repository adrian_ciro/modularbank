# REPORT MICRO-SERVICE 

### Description 
Report microservice keeps a replica of the data from account microservice.
This app subscribes itself to two queues in RabbitMQ and process all received messages, which will store in DB all data from accounts and transactions.
An account listener will process a msg coming from RabbitMQ with key "account.#"
A transaction listener will process a msg coming from RabbitMQ with key "transaction.#"


### Preconditions to run the app
- Installed Java 11
- Docker should be running with database postgres and rabbit containers running  


### How to run the application independently
Be sure you are in folder report and build the project

For windows\
`gradlew.bat build`

For unix\
`./gradlew build`

Then start up the application

For windows\
`gradlew.bat bootRun`

For unix\
`./gradlew bootRun`


### Database
In order to verify the DB is saving properly the information, please connect using an DB visualizer like DBeaver or DataGrip\
`jdbc:postgresql://localhost:9932/postgres`
user: postgres
pass: easypassword

### Remarks 
- The solution has the typical pattern of controllers (presentation layer), services (business rules) and model (data).
- It was my first time using RabbitMQ. The concept is very similar to kafka, but any feedback regarding the way I implemented it is welcome.
- Code coverage of tests is more than 80%.
