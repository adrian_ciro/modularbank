package co.modularbank.report.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.util.StreamUtils.copyToString;

import co.modularbank.report.base.BaseIntTest;
import co.modularbank.report.dao.Account;
import co.modularbank.report.dao.AccountDaoMapper;
import co.modularbank.report.dao.Transaction;
import co.modularbank.report.dao.TransactionDaoMapper;
import co.modularbank.report.dao.enums.TransactionDirection;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

class RabbitReceiverServiceIntTest extends BaseIntTest {

  @Value("classpath:Account.json")
  private Resource accountResource;
  @Value("classpath:AccountUpdated.json")
  private Resource accountUpdated;
  @Value("classpath:Transaction.json")
  private Resource transactionResource;

  @Autowired
  RabbitReceiverService rabbitReceiverService;
  @Autowired
  AccountDaoMapper accountDaoMapper;
  @Autowired
  TransactionDaoMapper transactionDaoMapper;

  @Test
  void receiveAccountAndCreateSuccessfully() {
    String message = getContent(accountResource);

    rabbitReceiverService.receiveAccount(message);

    Account accountInDb = accountDaoMapper.findById(2L);
    assertThat(accountInDb).isNotNull();
    assertThat(accountInDb.getCustomerId()).isEqualTo(2L);
    assertThat(accountInDb.getAmount()).isEqualByComparingTo(BigDecimal.valueOf(100));
  }

  @Test
  void receiveAccountAndUpdateSuccessfully() {
    String message = getContent(accountUpdated);

    rabbitReceiverService.receiveAccount(message);

    Account accountInDb = accountDaoMapper.findById(1L);
    assertThat(accountInDb).isNotNull();
    assertThat(accountInDb.getAmount()).isEqualByComparingTo(BigDecimal.valueOf(300));
  }

  @Test
  void receiveAccountAndProcessMessageFails() {
    assertThrows(Exception.class, () -> rabbitReceiverService.receiveAccount(null));
    assertThrows(Exception.class, () -> rabbitReceiverService.receiveAccount(""));
  }

  @Test
  void receiveTransactionAndCreateSuccessfully() {
    String message = getContent(transactionResource);

    rabbitReceiverService.receiveTransaction(message);

    Transaction transactionInDb = transactionDaoMapper.findById(1L);
    assertThat(transactionInDb).isNotNull();
    assertThat(transactionInDb.getAccountId()).isEqualTo(1L);
    assertThat(transactionInDb.getDirection()).isEqualTo(TransactionDirection.IN);
    assertThat(transactionInDb.getAmount()).isEqualByComparingTo(BigDecimal.valueOf(100));
  }

  private String getContent(Resource resource) {
    try {
      return copyToString(resource.getInputStream(), StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

}