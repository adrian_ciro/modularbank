package co.modularbank.report.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;

import co.modularbank.report.base.BaseUnitTest;
import co.modularbank.report.dao.Transaction;
import co.modularbank.report.dao.TransactionDaoMapper;
import co.modularbank.report.dao.enums.Currency;
import co.modularbank.report.dao.enums.TransactionDirection;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class TransactionServiceTest extends BaseUnitTest {

  private static final long ACCOUNT_ID = 1L;
  private static final long TRANSACTION_ID = 1L;

  @InjectMocks
  TransactionService transactionService;
  @Mock
  TransactionDaoMapper transactionDaoMapper;

  @Test
  void processTransactionThrowsExceptionFromDatabaseWhenInserting() throws Exception {
    Transaction transaction = getTransaction();
    doThrow(new RuntimeException()).when(transactionDaoMapper).save(transaction);

    assertThrows(Exception.class, () -> transactionService.processTransaction(transaction));
  }

  private Transaction getTransaction() {
    return new Transaction(TRANSACTION_ID, ACCOUNT_ID, BigDecimal.TEN, Currency.EUR, TransactionDirection.IN, "shop", BigDecimal.ONE);
  }
}