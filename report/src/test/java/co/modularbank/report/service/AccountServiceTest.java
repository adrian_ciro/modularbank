package co.modularbank.report.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import co.modularbank.report.base.BaseUnitTest;
import co.modularbank.report.dao.Account;
import co.modularbank.report.dao.AccountDaoMapper;
import co.modularbank.report.dao.enums.Currency;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class AccountServiceTest extends BaseUnitTest {

  private static final long CUSTOMER_ID = 1L;
  private static final long ACCOUNT_ID = 1L;

  @InjectMocks
  AccountService accountService;
  @Mock
  AccountDaoMapper accountDaoMapper;

  @Test
  void processAccountThrowsExceptionFromDatabaseWhenInserting() throws Exception {
    when(accountDaoMapper.findById(ACCOUNT_ID)).thenReturn(null);
    doThrow(new RuntimeException()).when(accountDaoMapper).save(getAccount());

    assertThrows(Exception.class, () -> accountService.processAccount(getAccount()));
  }

  @Test
  void processAccountThrowsExceptionFromDatabaseWhenUpdating() throws Exception {
    when(accountDaoMapper.findById(ACCOUNT_ID)).thenReturn(getAccount());
    doThrow(new RuntimeException()).when(accountDaoMapper).updateAmount(getAccount());

    assertThrows(Exception.class, () -> accountService.processAccount(getAccount()));
  }

  private Account getAccount() {
    return new Account(ACCOUNT_ID, CUSTOMER_ID,  "EE", BigDecimal.TEN, Currency.EUR);
  }

}