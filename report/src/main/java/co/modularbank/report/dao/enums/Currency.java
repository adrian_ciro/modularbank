package co.modularbank.report.dao.enums;

public enum Currency {
  EUR,
  SEK,
  GBP,
  USD
}
