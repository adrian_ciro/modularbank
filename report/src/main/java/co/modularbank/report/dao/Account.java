package co.modularbank.report.dao;

import co.modularbank.report.dao.enums.Currency;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Account implements Serializable {

  private Long id;
  private Long customerId;
  private String country;
  private BigDecimal amount;
  private Currency currency;

}
