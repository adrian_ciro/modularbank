package co.modularbank.report.dao.enums;

public enum TransactionDirection {
  IN,
  OUT
}
