package co.modularbank.report.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface AccountDaoMapper {

  @Select("select id, customerId, country, amount, currency from account where id = #{id}")
  Account findById(@Param("id") Long id);

  @Insert("insert into account (customerId, country, amount, currency) VALUES "
      + "( #{account.customerId}, #{account.country},#{account.amount},#{account.currency})")
  @Options(useGeneratedKeys = true, keyProperty = "id")
  void save(@Param("account") Account account) throws Exception;

  @Update("update account set amount= #{account.amount} where id = #{account.id}")
  void updateAmount(@Param("account") Account account) throws Exception;
}
