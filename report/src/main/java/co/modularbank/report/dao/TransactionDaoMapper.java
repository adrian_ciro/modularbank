package co.modularbank.report.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TransactionDaoMapper {

  @Select("select * from transaction where id = #{id}")
  Transaction findById(@Param("id") Long id);

  @Insert("insert into transaction (accountId, amount, currency, description, balanceAfter, direction) VALUES "
      + "(#{transaction.accountId},#{transaction.amount},#{transaction.currency},#{transaction.description}, "
      + "#{transaction.balanceAfter},#{transaction.direction})")
  @Options(useGeneratedKeys = true, keyProperty = "id")
  void save(@Param("transaction") Transaction transaction) throws Exception;
}
