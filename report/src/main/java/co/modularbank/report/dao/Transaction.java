package co.modularbank.report.dao;

import co.modularbank.report.dao.enums.Currency;
import co.modularbank.report.dao.enums.TransactionDirection;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Transaction implements Serializable {

  private Long id;
  private Long accountId;
  private BigDecimal amount;
  private Currency currency;
  private TransactionDirection direction;
  private String description;
  private BigDecimal balanceAfter;

}
