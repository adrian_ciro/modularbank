package co.modularbank.report.config;

import static co.modularbank.report.util.RabbitMqConstants.EXCHANGE_TOPIC;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExchangeConfig {

  @Bean
  TopicExchange exchange() {
    return new TopicExchange(EXCHANGE_TOPIC);
  }

}
