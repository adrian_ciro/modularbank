package co.modularbank.report.config;

import static co.modularbank.report.util.RabbitMqConstants.ACCOUNT_QUEUE;
import static co.modularbank.report.util.RabbitMqConstants.ROUTING_KEY_ACCOUNT;

import co.modularbank.report.service.RabbitReceiverService;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AccountListenerConfig {

  @Bean
  Queue accountQueue() {
    return new Queue(ACCOUNT_QUEUE, false);
  }

  @Bean
  Binding bindingAccount(Queue accountQueue, TopicExchange exchange) {
    return BindingBuilder.bind(accountQueue).to(exchange).with(ROUTING_KEY_ACCOUNT);
  }

  @Bean
  SimpleMessageListenerContainer containerAccount(ConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapterAccount) {
    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    container.setConnectionFactory(connectionFactory);
    container.setQueueNames(ACCOUNT_QUEUE);
    container.setMessageListener(listenerAdapterAccount);
    return container;
  }

  @Bean
  MessageListenerAdapter listenerAdapterAccount(RabbitReceiverService receiver) {
    return new MessageListenerAdapter(receiver, "receiveAccount");
  }

}
