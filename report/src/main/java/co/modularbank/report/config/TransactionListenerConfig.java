package co.modularbank.report.config;

import static co.modularbank.report.util.RabbitMqConstants.ROUTING_KEY_TRANSACTION;
import static co.modularbank.report.util.RabbitMqConstants.TRANSACTION_QUEUE;

import co.modularbank.report.service.RabbitReceiverService;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TransactionListenerConfig {

  @Bean
  Queue transactionQueue() {
    return new Queue(TRANSACTION_QUEUE, false);
  }

  @Bean
  Binding bindingTransaction(Queue transactionQueue, TopicExchange exchange) {
    return BindingBuilder.bind(transactionQueue).to(exchange).with(ROUTING_KEY_TRANSACTION);
  }

  @Bean
  SimpleMessageListenerContainer containerTransaction(ConnectionFactory connectionFactory,
      MessageListenerAdapter listenerAdapterTransaction) {
    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    container.setConnectionFactory(connectionFactory);
    container.setQueueNames(TRANSACTION_QUEUE);
    container.setMessageListener(listenerAdapterTransaction);
    return container;
  }

  @Bean
  MessageListenerAdapter listenerAdapterTransaction(RabbitReceiverService receiver) {
    return new MessageListenerAdapter(receiver, "receiveTransaction");
  }

}
