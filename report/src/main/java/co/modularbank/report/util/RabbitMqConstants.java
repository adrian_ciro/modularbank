package co.modularbank.report.util;

import static lombok.AccessLevel.PRIVATE;

import lombok.NoArgsConstructor;

@NoArgsConstructor(access = PRIVATE)
public class RabbitMqConstants {

  public static final String EXCHANGE_TOPIC = "co.modularbank";
  public static final String ROUTING_KEY_ACCOUNT = "account.#";
  public static final String ROUTING_KEY_TRANSACTION = "transaction.#";
  public static final String ACCOUNT_QUEUE = "account-queue";
  public static final String TRANSACTION_QUEUE = "transaction-queue";

}
