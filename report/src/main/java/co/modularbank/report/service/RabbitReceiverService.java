package co.modularbank.report.service;

import co.modularbank.report.dao.Account;
import co.modularbank.report.dao.Transaction;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitReceiverService {

  private static final String CONVERTING_OBJECT_ERROR_MSG = "Error converting object from rabbitMq";

  private final ObjectMapper objectMapper;
  private final AccountService accountService;
  private final TransactionService transactionService;

  public void receiveAccount(String message) {
    log.info("Received account: " + message);
    accountService.processAccount(getDomainObject(message, Account.class));
  }

  public void receiveTransaction(String message) {
    log.info("Received transaction: " + message);
    transactionService.processTransaction(getDomainObject(message, Transaction.class));
  }

  private <T> T getDomainObject(String msg, Class<T> eventClass) {
    try {
      return objectMapper.readValue(msg, eventClass);
    } catch (Exception e) {
      log.error(CONVERTING_OBJECT_ERROR_MSG + " :{} " + msg, e.getMessage());
      throw new IllegalArgumentException(CONVERTING_OBJECT_ERROR_MSG);
    }
  }

}
