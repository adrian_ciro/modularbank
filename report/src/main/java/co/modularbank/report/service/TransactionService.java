package co.modularbank.report.service;

import co.modularbank.report.dao.Transaction;
import co.modularbank.report.dao.TransactionDaoMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionService {

  private final TransactionDaoMapper transactionDaoMapper;

  public void processTransaction(Transaction transaction) {
    save(transaction);
  }

  private void save(Transaction transaction) {
    try {
      transactionDaoMapper.save(transaction);
    } catch (Exception e) {
      log.error("Error saving transaction for accountId " + transaction.getAccountId());
      throw new RuntimeException("Error while inserting transaction in database");
    }
  }
}
