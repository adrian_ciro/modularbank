package co.modularbank.report.service;

import static java.util.Objects.isNull;

import co.modularbank.report.dao.Account;
import co.modularbank.report.dao.AccountDaoMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

  private final AccountDaoMapper accountDaoMapper;

  public void processAccount(Account accountToSave) {
    if (isNull(getAccountsById(accountToSave.getId()))) {
      saveAccount(accountToSave);
    } else {
      updateAccount(accountToSave);
    }
  }

  private Account getAccountsById(Long id) {
    return accountDaoMapper.findById(id);
  }

  private void updateAccount(Account account) {
    try {
      accountDaoMapper.updateAmount(account);
    } catch (Exception e) {
      log.error("Error updating account " + account.getId());
      throw new RuntimeException("Error while inserting account in database");
    }
  }

  private void saveAccount(Account account) {
    try {
      accountDaoMapper.save(account);
    } catch (Exception e) {
      log.error("Error saving account for customer " + account.getCustomerId());
      throw new RuntimeException("Error while inserting account in database");
    }
  }
}
